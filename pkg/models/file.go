/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package models

import "github.com/michiwend/gomusicbrainz"

// File is an individual file uploaded to the Marmota Network
type File struct {
	// ID is the IPFS identifier for the file. The ID uses the Cid format. The Cid version is v1. https://github.com/multiformats/cid
	ID string

	// MBID is the MusicBrainz track ID
	MBID gomusicbrainz.MBID

	// ServerURL is the URL of the server where it was uploaded
	ServerURL string

	// TODO: Rating

	// Bitrate is the Bitrate of the recording
	Bitrate int
}
