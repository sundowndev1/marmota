/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package models

import "time"

// Instance is an individual instance of Marmota. This is used for discovery
type Instance struct {
	// URL is the URL of the instance
	URL string `gorm:"primary_key"`

	// UpdatedAt is the time where the relay data of this server was last updated
	UpdatedAt time.Time

	// Blocked sets if the instance is blocked or not
	Blocked bool
}
