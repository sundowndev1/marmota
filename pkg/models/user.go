/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package models

import "time"

// User is the model for an user of Marmota
type User struct {
	// ID is the unique identifier of the user. It's the username
	ID string

	// Name is the name that is going to be displayed in the user profile
	Name string

	// Authentication

	// Profile Picture

	// LikedSongs

	// Playlists

	// Status: gorm ignore

	// LastPlayedSong

	// CreatedAt contains the time when the user whas first created
	CreatedAt time.Time

	// UpdatedAt contains the time when the user was last updated
	UpdatedAt time.Time

	// DeletedAt contains the time when the user was deleted
	DeletedAt *time.Time
}
