/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package musicbrainz_test

import (
	"testing"

	"gitlab.com/nefix/marmota/pkg/fs"
	"gitlab.com/nefix/marmota/pkg/musicbrainz"
	"gitlab.com/nefix/marmota/pkg/utils/tests"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
)

func TestInit(t *testing.T) {
	assert := assert.New(t)

	t.Run("should work as expected", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()
		tests.InitCfg()

		assert.Nil(musicbrainz.Cli)
		musicbrainz.Init()
		assert.NotNil(musicbrainz.Cli)
	})

	t.Run("should exit if there's an error initializing the Go MusicBrainz client", func(t *testing.T) {
		fs.FS = afero.NewOsFs()
		assert.Nil(afero.WriteFile(fs.FS, "config.toml", []byte(`[marmota]
url = ":"`), 0644))

		tests.AssertExits(t, musicbrainz.Init)

		assert.Nil(fs.FS.Remove("config.toml"))
	})
}
