/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package musicbrainz

import (
	"encoding/json"
	"fmt"

	"github.com/michiwend/gomusicbrainz"
)

// Track is a specific song in a specific release from an specific artist
type Track struct {
	MBID              gomusicbrainz.MBID
	Title             string
	ArtistName        string
	ReleaseArtistName string
	ReleaseName       string
	Length            int
}

// getTrackResponse is the JSON response from the Music Brainz API
type getTrackResponse struct {
	Releases []struct {
		Title        string `json:"title"`
		ArtistCredit []struct {
			JoinPhrase string `json:"joinphrase"`
			Name       string `json:"name"`
		} `json:"artist-credit"`
		Media []struct {
			Tracks []struct {
				ID           gomusicbrainz.MBID `json:"id"`
				Title        string             `json:"title"`
				Length       int                `json:"length"`
				ArtistCredit []struct {
					JoinPhrase string `json:"joinphrase"`
					Name       string `json:"name"`
				} `json:"artist-credit"`
			} `json:"tracks"`
		} `json:"media"`
	} `json:"releases"`
}

// GetTrack returns a track by it's MBID
func GetTrack(trackID gomusicbrainz.MBID) (Track, error) {
	rsp, err := restCli.Get("release", map[string]string{
		"fmt":   "json",
		"track": string(trackID),
		"inc":   "artist-credits",
	})
	if err != nil {
		return Track{}, fmt.Errorf("error calling the API: %v", err)
	}

	decodedRsp := &getTrackResponse{}
	err = json.Unmarshal(rsp, &decodedRsp)
	if err != nil {
		return Track{}, fmt.Errorf("error unmarshaling the response: %v", err)
	}

	var track Track

	for _, r := range decodedRsp.Releases {
		var releaseArtistName string
		for _, a := range r.ArtistCredit {
			releaseArtistName += a.Name + a.JoinPhrase
		}

		for _, m := range r.Media {
			for _, t := range m.Tracks {
				if t.ID == trackID {
					var artistName string
					for _, a := range t.ArtistCredit {
						artistName += a.Name + a.JoinPhrase
					}

					track = Track{
						MBID:              t.ID,
						Title:             t.Title,
						ArtistName:        artistName,
						ReleaseArtistName: releaseArtistName,
						ReleaseName:       r.Title,
						Length:            t.Length,
					}
				}
			}
		}
	}

	return track, nil
}
