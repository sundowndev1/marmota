/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package musicbrainz

import (
	"os"

	"gitlab.com/nefix/marmota/pkg/cfg"
	"gitlab.com/nefix/marmota/pkg/utils/rest"

	"github.com/michiwend/gomusicbrainz"
	jww "github.com/spf13/jwalterweatherman"
)

// Client is the interface of the MusicBrainz API client
type Client interface {
	SearchArtist(searchTerm string, limit, offset int) (*gomusicbrainz.ArtistSearchResponse, error)
	SearchRecording(searchTerm string, limit, offset int) (*gomusicbrainz.RecordingSearchResponse, error)
	SearchRelease(searchTerm string, limit, offset int) (*gomusicbrainz.ReleaseSearchResponse, error)
}

// Cli is the actual client that Marmota is going to use to query MusicBrainz
var Cli Client

// restCli is the client used locally for more advanced queries
var restCli *rest.Client

// Init creates the MusicBrainz client
func Init() {
	var err error
	Cli, err = gomusicbrainz.NewWS2Client(
		cfg.Config.GetString("musicbrainz.URL"),
		"Marmota",
		cfg.VERSION,
		"https://gitlab.com/marmota",
	)
	if err != nil {
		jww.FATAL.Printf("error creating the MusicBrainz client: %v", err)
		os.Exit(1)
	}

	restCli, err = rest.NewClient(cfg.Config.GetString("musicbrainz.URL"))
	if err != nil {
		jww.FATAL.Printf("error creating the MusicBrainz client: %v", err)
		os.Exit(1)
	}
}
