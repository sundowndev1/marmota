/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tests

import (
	"gitlab.com/nefix/marmota/pkg/db"

	"github.com/jinzhu/gorm"
	// SQLite3
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// InitDB initializes an in-memory database
func InitDB() error {
	var err error

	db.DB, err = gorm.Open("sqlite3", ":memory:")
	db.AutoMigrate()

	return err
}
