/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tests

import (
	"gitlab.com/nefix/marmota/pkg/cfg"
	"gitlab.com/nefix/marmota/pkg/fs"

	"github.com/spf13/afero"
	"github.com/spf13/viper"
)

// InitCfg sets up Marmota's configuration and reads it. For testing only
func InitCfg() error {
	cfg.Config = viper.New()

	cfg.SetDefaults()
	cfg.Config.SetFs(fs.FS)

	if err := fs.FS.MkdirAll("/etc/marmota", 0755); err != nil {
		return err
	}

	if err := afero.WriteFile(fs.FS, "/etc/marmota/config.toml", []byte(""), 0644); err != nil {
		return err
	}

	if err := cfg.Config.ReadInConfig(); err != nil {
		return err
	}

	cfg.Config.WatchConfig()

	return nil
}
