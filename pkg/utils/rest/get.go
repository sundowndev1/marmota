/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package rest

import (
	"fmt"
	"io/ioutil"
)

// Get makes a Get request to the endpoint specified
func (c *Client) Get(endpoint string, args map[string]string) ([]byte, error) {
	rsp, err := c.client.Get(c.fullURL(endpoint, args))
	if err != nil {
		return nil, fmt.Errorf(`error getting "%s": %v`, endpoint, err)
	}

	if rsp.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP Code is %d", rsp.StatusCode)
	}

	body, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading the response body: %v", err)
	}
	rsp.Body.Close()

	return body, nil
}
