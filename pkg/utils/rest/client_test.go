/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package rest_test

import (
	"testing"

	"gitlab.com/nefix/marmota/pkg/utils/rest"

	"github.com/stretchr/testify/assert"
)

func TestNewClient(t *testing.T) {
	assert := assert.New(t)

	t.Run("shouldn't return an error if works correctly", func(t *testing.T) {
		cli, err := rest.NewClient("https://nefixestrada.com")
		assert.Nil(err)
		assert.NotEqual(&rest.Client{}, cli)
	})

	t.Run("should return an error if the URL is incorrect", func(t *testing.T) {
		expectedErr := "error parsing client URL: parse :: No gods! No managers! ::: missing protocol scheme"

		cli, err := rest.NewClient(":: No gods! No managers! ::")
		assert.EqualError(err, expectedErr)
		assert.Equal(&rest.Client{}, cli)
	})
}
