/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ipfs

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/nefix/marmota/pkg/cfg"

	shell "github.com/ipfs/go-ipfs-api"
	jww "github.com/spf13/jwalterweatherman"
)

// TODO: Test this package

// Cli is the client available for use in other packages
var Cli Client

// Client is the interface for the IPFS HTTP API client
type Client interface {
	Add(path string, pin bool) (cid string, err error)
}

// Init creates the IPFS client
func Init() {
	sh := shell.NewShell(
		fmt.Sprintf("%s:%d",
			cfg.Config.GetString("ipfs.host"),
			cfg.Config.GetInt("ipfs.port"),
		),
	)

	_, _, err := sh.Version()
	if err != nil {
		jww.FATAL.Printf("error creating the IPFS client: %v", err)
		os.Exit(1)
	}

	Cli = &HTTPCli{
		sh: sh,
	}
}

// HTTPCli is the HTTP client for the IPFS API
type HTTPCli struct {
	sh *shell.Shell
}

// Add adds a file to IPFS
func (h *HTTPCli) Add(path string, pin bool) (string, error) {
	f, err := os.Open(path)
	if err != nil {
		jww.ERROR.Printf("error adding %s to IPFS: error reading the file: %v", path, err)
		return "", fmt.Errorf("error adding %s to IPFS: error reading the file: %v", path, err)
	}

	cid, err := h.sh.AddWithOpts(bufio.NewReader(f), true, false)
	if err != nil {
		jww.ERROR.Printf("error adding %s to IPFS: %v", path, err)
		return "", fmt.Errorf("error adding %s to IPFS: %v", path, err)
	}

	return cid, err
}
