/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cfg

import (
	"os"

	"github.com/fsnotify/fsnotify"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"
)

// Config is the variable that contains Marmota's configuration
var Config *viper.Viper

// VERSION is the version of Marmota. Marmota uses Semantic Versioning
const VERSION string = "0.1.0"

// Init sets up Marmota's configuration and reads it
func Init() {
	Config = viper.New()

	SetDefaults()

	// this is covered, but since it tests os.Exit(1), it doesn't get marked as covered
	if err := Config.ReadInConfig(); err != nil {
		jww.FATAL.Printf("error reading the configuration: %v", err)
		os.Exit(1)
	}

	Config.WatchConfig()
	Config.OnConfigChange(func(e fsnotify.Event) {
		jww.INFO.Println("configuration file changed")
	})
}

// SetDefaults sets the default configurations for Viper
func SetDefaults() {
	Config.SetConfigName("config")
	Config.AddConfigPath(".")
	Config.AddConfigPath("$HOME/.config/marmota")
	Config.AddConfigPath("$HOME/.marmota")
	Config.AddConfigPath("/etc/marmota")

	Config.SetDefault("marmota", map[string]interface{}{
		"URL": "https://marmota.example.org",
	})
	Config.SetDefault("MusicBrainz", map[string]interface{}{
		"URL": "https://musicbrainz.org/ws/2",
	})
	Config.SetDefault("db", map[string]interface{}{
		"type":     "sqlite",
		"dir":      "/var/lib/marmota/database",
		"username": "marmota",
		"password": "P4$$w0rd!",
		"host":     "localhost",
		"port":     5432,
		"name":     "marmota",
	})
	Config.SetDefault("library", map[string]interface{}{
		"dir": "/srv/marmota/library",
	})
	Config.SetDefault("ipfs", map[string]interface{}{
		"host": "localhost",
		"port": 5001,
	})
}
